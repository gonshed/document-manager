package org.smirnov.documentmanager.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.smirnov.documentmanager.controller.CleanFieldsController;
import org.smirnov.documentmanager.controller.DocumentTypeController;
import org.smirnov.documentmanager.controller.SaveDocumentController;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class to start application
 */
public class DocumentManager extends Application {
    public static final String MAIN_FXML = "main.fxml";
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", new Locale("ru", "Ru"));

    /**base GUI elements*/
    private TitledPane documentTypesPane;
    private TitledPane editorialContentPane;
    private Label documentContentLabel;
    private TextArea documentBody;
    private TextField name;
    private DatePicker date;
    private Button cleanFieldsButton;
    private Button saveDocumentButton;

    private SaveDocumentController saveDocumentController;
    private CleanFieldsController cleanFieldsController;
    private DocumentTypeController documentTypeController;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        AnchorPane root = FXMLLoader.load(getClass().getResource(MAIN_FXML));

        Scene scene = new Scene(root);
        initialiseComponents(scene);
        initialiseCleanFieldsController();
        initialiseDocumentTypeController();
        initialiseSaveDocumentController();

        primaryStage.setScene(scene);
        primaryStage.setTitle(resourceBundle.getString("application.name"));
        primaryStage.show();
    }

    private void initialiseCleanFieldsController() {
        this.cleanFieldsController = new CleanFieldsController.Builder()
                .documentBody(this.documentBody)
                .name(this.name)
                .date(this.date)
                .cleanFieldButton(this.cleanFieldsButton)
                .build();
        this.cleanFieldsController.initialise();
    }

    private void initialiseSaveDocumentController() {
        this.saveDocumentController = new SaveDocumentController.Builder()
                .documentBody(this.documentBody)
                .name(this.name)
                .date(this.date)
                .saveFieldButton(this.saveDocumentButton)
                .documentTypeController(documentTypeController)
                .build();
        this.saveDocumentController.initialise();
    }

    private void initialiseDocumentTypeController() {
        this.documentTypeController = new DocumentTypeController();
        documentTypeController.setDocumentBody(documentBody);
        documentTypeController.setDocumentTypesPane(documentTypesPane);
        documentTypeController.initialise();
    }

    /**Method loads base elements and put their names*/
    private void initialiseComponents(Scene scene) {
        AnchorPane leftPane = (AnchorPane) ((SplitPane) scene.lookup("#mainSplitPane")).getItems().get(0);
        this.documentTypesPane = (TitledPane) leftPane.getChildren().get(0);
        this.documentTypesPane.setText(resourceBundle.getString("document.type.titled.pane.header"));
        AnchorPane rightPane = (AnchorPane) ((SplitPane) scene.lookup("#mainSplitPane")).getItems().get(1);
        this.editorialContentPane = (TitledPane) rightPane.lookup("#editorialContentTitledPane");
        this.editorialContentPane.setText(resourceBundle.getString("editorial.content.pane.header"));
        SplitPane rightSplitPane = (SplitPane) this.editorialContentPane.getContent();

        this.documentBody = (TextArea) rightSplitPane.getItems().get(0).lookup("#documentContent");
        this.documentContentLabel = (Label) rightSplitPane.getItems().get(0).lookup("#documentContentLabel");
        this.documentContentLabel.setText(resourceBundle.getString("document.body.header"));
        this.documentBody.setPromptText(resourceBundle.getString("document.body.preferred.text"));
        this.name = (TextField) rightSplitPane.getItems().get(1).lookup("#name");
        this.name.setPromptText(resourceBundle.getString("name.preferred.text"));
        this.date = (DatePicker) rightSplitPane.getItems().get(1).lookup("#date");
        this.date.setPromptText(resourceBundle.getString("data.preferred.text"));
        this.saveDocumentButton = (Button) rightSplitPane.getItems().get(1).lookup("#saveDocument");
        this.saveDocumentButton.setText(resourceBundle.getString("save.button.text"));
        this.cleanFieldsButton = (Button) rightSplitPane.getItems().get(1).lookup("#cleanContent");
        this.cleanFieldsButton.setText(resourceBundle.getString("clean.button.text"));
    }

    public TitledPane getDocumentTypesPane() {
        return documentTypesPane;
    }

    public TextArea getDocumentBody() {
        return documentBody;
    }

    public TextField getName() {
        return name;
    }

    public DatePicker getDate() {
        return date;
    }

    public Button getCleanFieldsButton() {
        return cleanFieldsButton;
    }

    public Button getSaveDocumentButton() {
        return saveDocumentButton;
    }
}
