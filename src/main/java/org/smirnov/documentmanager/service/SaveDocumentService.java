package org.smirnov.documentmanager.service;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.smirnov.documentmanager.type.DocumentContent;
import org.smirnov.documentmanager.type.DocumentType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

public class SaveDocumentService {

    private static final String FILE_EXTENSION = ".docx";
    private static final String BASE_TEMPLATES_DIRECTORY = "./templates/";

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", new Locale("ru", "Ru"));

    public void save(String documentName, DocumentType documentType, DocumentContent documentContent) throws IOException {
        XWPFDocument document;
        try(InputStream inputStream = FileUtils.openInputStream(new File(BASE_TEMPLATES_DIRECTORY + documentType.getFileName() + FILE_EXTENSION))) {
            document = new XWPFDocument(inputStream);
        }
        XWPFDocument resultDocument = (XWPFDocument)  replacePlaceholders(document, documentContent);

        String resultDirectory = resourceBundle.getString("result.directory");
        File directory = new File(resultDirectory);

        if (!directory.exists()) {
            directory.mkdir();
        }

        try (FileOutputStream out = new FileOutputStream( resultDirectory + "/" + documentName + FILE_EXTENSION)) {
            resultDocument.write(out);
        }
    }

    private IBody replacePlaceholders(IBody document, DocumentContent documentContent) {
        for (IBodyElement bodyElement : document.getBodyElements()) {
            if (bodyElement.getElementType().equals(BodyElementType.PARAGRAPH)) {
                replaceInParagraph((XWPFParagraph) bodyElement, documentContent);
            } else if (bodyElement.getElementType().equals(BodyElementType.TABLE)) {
                XWPFTable table = (XWPFTable) bodyElement;
                for (XWPFTableRow row : table.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        replacePlaceholders(cell, documentContent);
                    }
                }
            }
        }

        return document;
    }

    void replaceInParagraph(XWPFParagraph paragraph, DocumentContent documentContent) {
        for (XWPFRun run : paragraph.getRuns()) {
            String text = run.getText(run.getTextPosition());
            if (text.contains("${name}")) {
                run.setText(text.replace("${name}", documentContent.getName()), 0);
            }

            if (text.contains("${text}")) {
                run.setText(text.replace("${text}", documentContent.getBody()), 0);
            }

            if (text.contains("${date}")) {
                run.setText(text.replace("${date}", documentContent.getDate()), 0);
            }
        }
    }
}
