package org.smirnov.documentmanager.service;

import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.smirnov.documentmanager.type.DocumentType;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Creating radio buttons and edit buttons from DocumentType enum
 */
public class DocumentTypeListService {
    private static final String FILE_EXTENSION = ".docx";
    private static final String BASE_TEMPLATES_DIRECTORY = "./templates/";

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", new Locale("ru", "Ru"));

    public VBox prepareRadioButtonList(DocumentType[] documentTypeList, ToggleGroup toggleGroup) {
        VBox vbox = new VBox();
        vbox.setSpacing(10);
        for (DocumentType documentType : documentTypeList) {
            HBox hBox = new HBox();
            hBox.setSpacing(10);
            hBox.setAlignment(Pos.BASELINE_RIGHT);

            RadioButton radioButton = new RadioButton();
            radioButton.setToggleGroup(toggleGroup);
            radioButton.setAlignment(Pos.CENTER_RIGHT);
            radioButton.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
            radioButton.setText(resourceBundle.getString(documentType.getId() + ".label"));
            radioButton.setId(documentType.getId());

            //edit button opens to edit selected document
            Button buttonEdit = new Button();
            buttonEdit.setOnMouseClicked(event -> {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().open(new File(BASE_TEMPLATES_DIRECTORY + documentType.getFileName() + FILE_EXTENSION));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            buttonEdit.setAlignment(Pos.CENTER_RIGHT);
            buttonEdit.setText(resourceBundle.getString("button.edit.text"));

            hBox.getChildren().addAll(radioButton, buttonEdit);
            vbox.getChildren().add(hBox);
        }

        return vbox;
    }
}
