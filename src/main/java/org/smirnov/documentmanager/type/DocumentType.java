package org.smirnov.documentmanager.type;

import org.smirnov.documentmanager.exception.NotSelectedDocumentTypeException;

/**
 * All possible document types
 */
public enum DocumentType {
    CLAIM("claim", "CLAIM"),
    CONTRACT("contract", "CONTRACT"),
    NOTE("note", "NOTE");

    private String id;
    private String fileName;

    DocumentType(String id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public static DocumentType valueOfId(String id) {
        for (DocumentType type : values()) {
            if (type.getId().equals(id)) {
                return type;
            }
        }

        throw new NotSelectedDocumentTypeException();
    }

    public String getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }
}
