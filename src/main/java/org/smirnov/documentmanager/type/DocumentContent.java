package org.smirnov.documentmanager.type;

/**
 * Holds editorial parameters in one place
 */
public class DocumentContent {

    private String body;
    private String name;
    private String date;

    public String getBody() {
        return body;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public static class Builder {
        private String body;
        private String name;
        private String date;

        public Builder body(String body) {
            this.body = body;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder date(String date) {
            this.date = date;
            return this;
        }

        public DocumentContent build() {
            DocumentContent documentContent = new DocumentContent();
            documentContent.body = this.body;
            documentContent.name = this.name;
            documentContent.date = this.date;

            return documentContent;
        }
    }
}
