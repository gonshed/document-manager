package org.smirnov.documentmanager.controller;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public abstract class ButtonController {
    protected TextArea documentBody;
    protected TextField name;
    protected DatePicker date;
    protected Button button;

    public void initialise() {
        button.setOnMouseClicked(setupEvent());
    }

    public TextArea getDocumentBody() {
        return documentBody;
    }

    public TextField getName() {
        return name;
    }

    public DatePicker getDate() {
        return date;
    }

    public Button getButton() {
        return button;
    }

    abstract EventHandler<MouseEvent> setupEvent();
}
