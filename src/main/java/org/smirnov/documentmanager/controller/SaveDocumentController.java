package org.smirnov.documentmanager.controller;

import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import org.apache.commons.lang3.StringUtils;
import org.smirnov.documentmanager.exception.NotSelectedDocumentTypeException;
import org.smirnov.documentmanager.service.SaveDocumentService;
import org.smirnov.documentmanager.type.DocumentContent;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/** Controls replacing placeholders and saves new document*/
public class SaveDocumentController extends ButtonController {

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", new Locale("ru", "Ru"));

    private SaveDocumentService saveDocumentService = new SaveDocumentService();
    private DocumentTypeController documentTypeController;
    private AlertController alertController = new AlertController();

    @Override
    EventHandler<MouseEvent> setupEvent() {
        return event -> {
            DocumentContent documentContent = new DocumentContent.Builder()
                    .body(documentBody.getText())
                    .date(date.getValue() != null ? date.getValue().toString() : null)
                    .name(name.getText())
                    .build();

            if (StringUtils.isNotEmpty(documentContent.getBody()) && StringUtils.isNotEmpty(documentContent.getName())
                    && StringUtils.isNotEmpty(documentContent.getDate())) {
                try {
                    String documentName = resourceBundle.getString(documentTypeController.getSelectedDocumentType().getId() + ".label") + "_" + System.currentTimeMillis();
                    saveDocumentService.save(documentName,
                            documentTypeController.getSelectedDocumentType(),
                            documentContent);
                    //open window in successful saving case
                    alertController.prepareAlert(Alert.AlertType.INFORMATION,
                            resourceBundle.getString("alert.success.title"),
                            resourceBundle.getString("alert.document.prepared"),
                            documentName);
                    alertController.alertShow();
                } catch (IOException e) {
                    //open window in case some technical exceptions
                    alertController.prepareAlert(Alert.AlertType.ERROR,
                            resourceBundle.getString("alert.error.title"),
                            resourceBundle.getString("alert.unable.save.document"));
                    alertController.alertShow();
                } catch (NotSelectedDocumentTypeException e) {
                    // opens window if document type was not selected
                    alertController.prepareAlert(Alert.AlertType.ERROR,
                            resourceBundle.getString("alert.error.title"),
                            resourceBundle.getString("alert.not.selected.document.type"));
                    alertController.alertShow();
                }
            } else {
                // opens window if at least one field is empty
                alertController.prepareAlert(Alert.AlertType.ERROR,
                        resourceBundle.getString("alert.error.title"),
                        resourceBundle.getString("alert.not.all.fields"));
                alertController.alertShow();
            }
        };
    }

    public static class Builder {
        private TextArea documentBody;
        private TextField name;
        private DatePicker date;
        private Button saveFieldButton;
        private DocumentTypeController documentTypeController;

        public Builder documentBody(TextArea documentBody) {
            this.documentBody = documentBody;
            return this;
        }

        public Builder name(TextField name) {
            this.name = name;
            return this;
        }

        public Builder date(DatePicker date) {
            this.date = date;
            return this;
        }

        public Builder saveFieldButton(Button emptyFieldButton) {
            this.saveFieldButton = emptyFieldButton;
            return this;
        }

        public Builder documentTypeController(DocumentTypeController documentTypeController) {
            this.documentTypeController = documentTypeController;
            return this;
        }

        public SaveDocumentController build() {
            SaveDocumentController controller = new SaveDocumentController();
            controller.documentBody = this.documentBody;
            controller.name = this.name;
            controller.date = this.date;
            controller.button = this.saveFieldButton;
            controller.documentTypeController = this.documentTypeController;

            return controller;
        }
    }
}
