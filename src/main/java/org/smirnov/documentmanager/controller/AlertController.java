package org.smirnov.documentmanager.controller;

import javafx.scene.control.Alert;

/**Handle exceptions and errors. Shows additional window with message*/
public class AlertController {
    private Alert alert = new Alert(Alert.AlertType.NONE);

    /**Fill window with data*/
    public void prepareAlert(Alert.AlertType type, String title, String header) {
        alert.setAlertType(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(null);
    }

    public void prepareAlert(Alert.AlertType type, String title, String header, String content) {
        prepareAlert(type, title, header);
        alert.setContentText(content);
    }

    /**Show window*/
    public void alertShow() {
        alert.show();
    }
}
