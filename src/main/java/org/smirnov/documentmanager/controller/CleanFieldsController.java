package org.smirnov.documentmanager.controller;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**Clean fields body content, name and date*/
public class CleanFieldsController extends ButtonController {
    public static class Builder {
        private TextArea documentBody;
        private TextField name;
        private DatePicker date;
        private Button cleanFieldButton;

        public Builder documentBody(TextArea documentBody) {
            this.documentBody = documentBody;
            return this;
        }

        public Builder name(TextField name) {
            this.name = name;
            return this;
        }

        public Builder date(DatePicker date) {
            this.date = date;
            return this;
        }

        public Builder cleanFieldButton(Button emptyFieldButton) {
            this.cleanFieldButton = emptyFieldButton;
            return this;
        }

        public CleanFieldsController build() {
            CleanFieldsController controller = new CleanFieldsController();
            controller.documentBody = this.documentBody;
            controller.name = this.name;
            controller.date = this.date;
            controller.button = this.cleanFieldButton;

            return controller;
        }
    }

    protected EventHandler<MouseEvent> setupEvent() {
        return event -> {
            documentBody.setText("");
            name.setText("");
            date.setValue(null);
        };
    }
}
