package org.smirnov.documentmanager.controller;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.*;
import org.smirnov.documentmanager.exception.NotSelectedDocumentTypeException;
import org.smirnov.documentmanager.service.DocumentTypeListService;
import org.smirnov.documentmanager.type.DocumentType;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Controls preparation for document types radio buttons
 */
public class DocumentTypeController {

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("translations", new Locale("ru", "Ru"));

    private DocumentTypeListService service = new DocumentTypeListService();

    private TextArea documentBody;
    private TitledPane documentTypesPane;
    private final ToggleGroup toggleGroup = new ToggleGroup(); // groups all radio buttons in document type section
    private AlertController alertController = new AlertController();

    public void initialise() {
        documentTypesPane.setContent(service.prepareRadioButtonList(DocumentType.values(), toggleGroup));
        toggleGroup.selectedToggleProperty().addListener(setupListener());
    }

    /** replaces document body with default text by selected document type*/
    protected ChangeListener<Toggle> setupListener() {
        return (observable, oldValue, newValue) -> {
            if (getSelectedDocumentType() != null) {
                documentBody.setText(resourceBundle.getString(getSelectedDocumentType().getId() + ".template.text"));
            } else {
                alertController.prepareAlert(Alert.AlertType.ERROR,
                        resourceBundle.getString("alert.error.title"),
                        resourceBundle.getString("alert.not.selected.document.type"));
                alertController.alertShow();
            }
        };
    }

    public TextArea getDocumentBody() {
        return documentBody;
    }

    public void setDocumentBody(TextArea documentBody) {
        this.documentBody = documentBody;
    }

    public TitledPane getDocumentTypesPane() {
        return documentTypesPane;
    }

    public void setDocumentTypesPane(TitledPane documentTypesPane) {
        this.documentTypesPane = documentTypesPane;
    }

    public DocumentType getSelectedDocumentType() {
        if (toggleGroup.selectedToggleProperty().getValue() != null) {
            return DocumentType.valueOfId(((RadioButton) toggleGroup.selectedToggleProperty().getValue()).getId());
        }

        throw new NotSelectedDocumentTypeException();
    }
}
