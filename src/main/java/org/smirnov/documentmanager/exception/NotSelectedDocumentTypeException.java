package org.smirnov.documentmanager.exception;

public class NotSelectedDocumentTypeException extends RuntimeException {
    public NotSelectedDocumentTypeException() {
        super();
    }

    public NotSelectedDocumentTypeException(String message) {
        super(message);
    }
}
